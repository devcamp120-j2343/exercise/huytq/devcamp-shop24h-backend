const express = require("express");

const orderDetailMiddleware = require("../middlewares/orderDetail.middleware");
const orderDetailController = require("../controllers/orderDetail.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL OrderDetail : ", req.url);

    next();
});

router.get("/", orderDetailMiddleware.getAllOrderDetailMiddleware, orderDetailController.getAllOrderDetail)

router.post("/", orderDetailMiddleware.createOrderDetailMiddleware, orderDetailController.createOrderDetail);

router.get("/:orderDetailId", orderDetailMiddleware.getDetailOrderDetailMiddleware, orderDetailController.getOrderDetailById);

router.put("/:orderDetailId", orderDetailMiddleware.updateOrderDetailMiddleware, orderDetailController.updateOrderDetail);

router.delete("/:orderDetailId", orderDetailMiddleware.deleteOrderDetailMiddleware, orderDetailController.deleteOrderDetail);


module.exports = router;
