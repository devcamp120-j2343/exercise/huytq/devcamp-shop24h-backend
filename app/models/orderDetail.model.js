//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const orderDetailSchema = new Schema({
    _id: mongoose.Types.ObjectId,
   
    product:[
        {
            type:mongoose.Types.ObjectId,
            ref:"product"
        }
    ],
    quantity: {
        type:Number,
        default:0
    }

});

//b4 biên dịch schema thành model
module.exports = mongoose.model("orderdetail",orderDetailSchema)
