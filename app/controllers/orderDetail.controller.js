//import thư viện mongoose
const mongoose = require('mongoose');

// import OrderDetail model
const orderDetailModel = require('../models/orderDetail.model');

//import order model
const orderModel = require('../models/order.model');

// const create review 
const createOrderDetail = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        orderId,
        product,
        quantity,
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Order ID is not valid"
        })
    }
    if (quantity === undefined || !(Number.isInteger(quantity))) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "quantity is not valid"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(product)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Id is not valid"
        })
    }

    // B3: Thao tác với CSDL
    var newOrderDetail = {
        _id: new mongoose.Types.ObjectId(),
        product: new mongoose.Types.ObjectId(product),
        quantity,
    }

    try {
        const createdOrderDetail = await orderDetailModel.create(newOrderDetail);
        
        const updatedOrder = await orderModel.findByIdAndUpdate(orderId, {
            $push: { orderDetails: createdOrderDetail._id }
        })

        return res.status(201).json({
            status: "Create OrderDetail successfully",
            order: updatedOrder,
            data: createdOrderDetail
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllOrderDetail = async (req, res) => {
    //B1: thu thập dữ liệu
    const orderId = req.query.orderId;
    //B2: kiểm tra
    if (orderId !== undefined && !mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "orderId  is not valid"
        })
    }
    //B3: thực thi model
    try {
        if(orderId === undefined) {
            const orderDetailList = await orderDetailModel.find().populate([
                {
                    path: "product",
                    model: "product",
                    populate: {
                        path: "type",
                        model: "productType",
                        select: "name description",
                    }
                },
            ]);

            if(orderDetailList && orderDetailList.length > 0) {
                return res.status(200).json({
                    status: "Get all OrderDetail  sucessfully",
                    data: orderDetailList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any OrderDetail "
                })
            }
        } else {
            const orderInfo = await orderModel.findById(orderId).populate([
                {
                    path: "product",
                    model: "product",
                    populate: {
                        path: "type",
                        model: "productType",
                        select: "name description",
                    }
                },
            ]);

            return res.status(200).json({
                status: "Get all order detail of order sucessfully",
                data: orderInfo.orderDetails
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getOrderDetailById = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderDetailId = req.params.orderDetailId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const orderDetailInfo = await orderDetailModel.findById(orderDetailId).populate([
            {
                path: "product",
                model: "product",
                populate: {
                    path: "type",
                    model: "productType",
                    select: "name description",
                }
            },
        ]);

        if (orderDetailInfo) {
            return res.status(200).json({
                status:"Get orderDetail by id sucessfully",
                data: orderDetailInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any OrderDetail"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateOrderDetail = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderDetailId = req.params.orderDetailId;

    const {
        product,
        quantity,} = req.body;

    //B2: kiểm tra dữ liệu
    if (quantity === undefined || !(Number.isInteger(quantity))) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "quantity is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(product)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "Id is not valid"
        })
    }

    //B3: thực thi model
    try {
        let updateOrderDetail = {
            product: new mongoose.Types.ObjectId(product),
            quantity
        }

        const updatedOrderDetail = await orderDetailModel.findByIdAndUpdate(orderDetailId, updateOrderDetail).populate([
            {
                path: "product",
                model: "product",
                populate: {
                    path: "type",
                    model: "productType",
                    select: "name description",
                }
            },
        ]);
        if (updatedOrderDetail) {
            return res.status(200).json({
                status:"Update OrderDetail sucessfully",
                data: updatedOrderDetail
            })
        } else {
            return res.status(404).json({
                status:"Not found any OrderDetail"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteOrderDetail = async (req, res) => {
    //B1: Thu thập dữ liệu
    var orderDetailId = req.params.orderDetailId;
    var orderId = req.query.orderId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedOrderDetail = await orderDetailModel.findByIdAndDelete(orderDetailId).populate([
            {
                path: "product",
                model: "product",
                populate: {
                    path: "type",
                    model: "productType",
                    select: "name description",
                }
            },
        ]);
        // Nếu có orderId thì xóa thêm (optional)
        if (orderId !== undefined) {
            await orderModel.findByIdAndUpdate(orderId, {
                $pull: { orderDetails: orderDetailId }
            })
        }
        if (deletedOrderDetail) {
            return res.status(200).json({
                status:"Delete OrderDetail sucessfully",
                data: deletedOrderDetail
            })
        } else {
            return res.status(404).json({
                status:"Not found any OrderDetail"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllOrderDetail,
    createOrderDetail,
    getOrderDetailById,
    updateOrderDetail,
    deleteOrderDetail
}

