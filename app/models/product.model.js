//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const productSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        require: true
    },
    description: {
        type: String,
        require: true  
    },
    type:[
        {
            type:mongoose.Types.ObjectId,
            ref:"producttype"
        }
    ],
    imageUrl: {
        type: String,
        require: true
    },
    buyPrice: {
        type: Number,
        require: true
    },
    promotionPrice: {
        type: Number,
        require: true,
    },
    amount : {
        type: Number,
        default: 0
    }

});

//b4 biên dịch schema thành model
module.exports = mongoose.model("product",productSchema)
