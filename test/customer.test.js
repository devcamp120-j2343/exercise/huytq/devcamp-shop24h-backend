const expect = require("chai").expect;
const request = require("supertest");
const customerModel = require("../app/models/customer.model");
const app = require("../index");
const mongoose = require('mongoose');

describe("/api/v1/customers", () => {
    before(async () => {
        await customerModel.deleteMany({});
    });

    // after(async () => {
    //     mongoose.disconnect();
    // });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/Shop24h-Project");
    });

    describe("GET /", () => {
        it("should return all customer", async () => {
            const productType = [
                { fullName:"HuyTran",
                    phone:"0359302461",
                    email:"huy@gmail.com",
                    address:"q7",
                    city:"TPHCM",
                    country:"vietnam",
                    orders: [],},
            ];
            await customerModel.insertMany(productType);
            const res = await request(app).get("/api/v1/customers");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(1);
        });
    });

    describe("GET /:customerId", () => {
        it("should return a customer if valid id is passed", async () => {
            const customer = {
            _id:new mongoose.Types.ObjectId(),
            fullName:"Ken",
            phone:"0359302451",
            email:"ken@gmail.com",
            address:"tay ho",
            city:"Hanoi",
            country:"vietnam",
            orders: [],
        };
            await customerModel.create(customer);
            const res = await request(app).get("/api/v1/customers/" + customer._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("fullName", customer.fullName);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/api/v1/customers/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/api/v1/customers/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return customer when the all request body is valid", async () => {
            const res = await request(app)
                .post("/api/v1/customers")
                .send({ 
                    fullName:"Lionel Messi",
                    phone:"0359302441",
                    email:"m10@gmail.com",
                    address:"mai dong",
                    city:"Hanoi",
                    country:"vietnam",
                    orders: [], 
                });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("fullName", "Lionel Messi");
            expect(data.data).to.have.property("phone", "0359302441");

            const customer = await customerModel.findOne({ email: 'm10@gmail.com' });
            expect(customer.address).to.equal('mai dong');
        });
    });

    describe("PUT /:customerId", () => {
        it("should update the existing customer and return 200", async () => {
            const customer = {
                _id:new mongoose.Types.ObjectId(),
                fullName:"Nguyen Xuan Phong",
                phone:"0359302431",
                email:"phonggay@gamil.com",
                address:"truong dinh",
                city:"Hanoi",
                country:"vietnam",
                orders: [],
            };
            await customerModel.create(customer);

            const res = await request(app)
                .put("/api/v1/customers/" + customer._id)
                .send({ fullName:"Nguyen Xuan Phong",
                phone:"0359302431",
                email:"phong@gmail.com",
                address:"truong dinh",
                city:"Hanoi",
                country:"vietnam",
                orders: [], });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await customerModel.findById(customer._id).exec();
            expect(dataAfterUpdate).to.have.property("email", "phong@gamil.com");
        });
    });

    let customerId = '';
    describe("DELETE /:customerId", () => {
        it("should delete requested id and return response 200", async () => {
            const customer = {
                _id:new mongoose.Types.ObjectId(),
                fullName:"Nguyen Ngoc Long",
                phone:"0359302421",
                email:"long@gmail.com",
                address:"lac trung",
                city:"Hanoi",
                country:"vietnam",
                orders: [],
            };
            await customerModel.create(customer);
            customerId = customer._id;
            const res = await request(app).delete("/api/v1/customers/" + customerId);
            expect(res.status).to.be.equal(200);
        });

        it("should return 404 when deleted customer is requested", async () => {
            let res = await request(app).get("/api/v1/customers/" + customerId);
            expect(res.status).to.be.equal(404);
        });
    });
});