//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const customerSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true,
        unique: true  
    },
    email: {
        type:String,
        require:true,
        unique:true
    },
    address: {
        type:String,
        require:true,
    },
    city: {
        type:String,
        require: true
    },
    country: {
        type: String,
        require: true
    },
    orders:[
        {
            type:mongoose.Types.ObjectId,
            ref:"order"
        }
    ],

});

//b4 biên dịch schema thành model
module.exports = mongoose.model("customer",customerSchema)
