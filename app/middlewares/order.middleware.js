const getAllOrderMiddleware = (req, res, next) => {
    console.log("Get all Order  Middleware");

    next();
}

const createOrderMiddleware = (req, res, next) => {
    console.log("Create Order  Middleware");

    next();
}

const getDetailOrderMiddleware = (req, res, next) => {
    console.log("Get detail Order  Middleware");

    next();
}

const updateOrderMiddleware = (req, res, next) => {
    console.log("Update Order  Middleware");

    next();
}

const deleteOrderMiddleware = (req, res, next) => {
    console.log("Delete Order  Middleware");

    next();
}

module.exports = {
    getAllOrderMiddleware,
    createOrderMiddleware,
    getDetailOrderMiddleware,
    updateOrderMiddleware,
    deleteOrderMiddleware
}

