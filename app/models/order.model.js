//b1 khai báo thư viện mongoose
const mongoose = require('mongoose')

//b2 khai báo class schema
const Schema = mongoose.Schema

//b3 khởi tạo schema với các thuộc tính collection
const orderSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    orderDate: {
        type: Date,
        default: Date.now()
    },
    shippedDate: {
        type: Date,
        default: Date.now()  
    },
    note: {
        type:String,
    },
    cost: {
        type:Number,
        default: 0
    },
    orderDetails:[
        {
            type:mongoose.Types.ObjectId,
            ref:"orderdetail"
        }
    ],

});

//b4 biên dịch schema thành model
module.exports = mongoose.model("order",orderSchema)
