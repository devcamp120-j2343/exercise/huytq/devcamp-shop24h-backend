//import thư viện mongoose
const mongoose = require('mongoose');

// import Order model
const orderModel = require('../models/order.model');

//import customer model
const customerModel = require('../models/customer.model');

// const create review 
const createOrder = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        customerId,
        orderDate,
        shippedDate,
        note,
        orderDetails,
        cost,
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "customer ID is not valid"
        })
    }
    if (cost === undefined || !(Number.isInteger(cost) && cost > 0 )) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "cost is not valid"
        })
    }

    

    // B3: Thao tác với CSDL
    var newOrder = {
        _id: new mongoose.Types.ObjectId(),
        orderDate,
        shippedDate,
        note,
        orderDetails,
        cost,
    }

    try {
        const createdOrder = await orderModel.create(newOrder);
        
        const updatedCustomer = await customerModel.findByIdAndUpdate(customerId, {
            $push: { orders: createdOrder._id }
        })

        return res.status(201).json({
            status: "Create order successfully",
            customer: updatedCustomer,
            data: createdOrder
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllOrder = async (req, res) => {
    //B1: thu thập dữ liệu
    const customerId = req.query.customerId;
    //B2: kiểm tra
    if (customerId !== undefined && !mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "customerId  is not valid"
        })
    }
    //B3: thực thi model
    try {
        if(customerId === undefined) {
            const orderList = await orderModel.find().populate([{
                    path: "orderDetails",
                    model: "orderDetail",
                    populate: {
                        path: "product",
                        model: "product",
                        select: "name amount",
                    }
            }]);

            if(orderList && orderList.length > 0) {
                return res.status(200).json({
                    status: "Get all order  sucessfully",
                    data: orderList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any order "
                })
            }
        } else {
            const customerInfo = await customerModel.findById(customerId).populate([{
                    path: "orderDetails",
                    model: "orderDetail",
                    populate: {
                        path: "product",
                        model: "product",
                        select: "name amount",
                    }
            }]);

            return res.status(200).json({
                status: "Get all order of customer sucessfully",
                data: customerInfo.orders
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getOrderById = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderId = req.params.orderId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const orderInfo = await orderModel.findById(orderId).populate([
            {
                path: "orderDetails",
                model: "orderDetail",
                populate: {
                    path: "product",
                    model: "product",
                    select: "name amount",
                }
            },
        ]);

        if (orderInfo) {
            return res.status(200).json({
                status:"Get order by id sucessfully",
                data: orderInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateOrder = async (req, res) => {
    //B1: thu thập dữ liệu
    var orderId = req.params.orderId;

    const {orderDate,
        shippedDate,
        note,
        orderDetails,
        cost,} = req.body;

    //B2: kiểm tra dữ liệu
    if (cost === undefined || !(Number.isInteger(cost) && cost > 0 )) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "cost is not valid"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        let updateOrder = {
            orderDate,
            shippedDate,
            note,
            orderDetails,
            cost,
        }

        const updatedOrder = await orderModel.findByIdAndUpdate(orderId, updateOrder).populate([
            {
                path: "orderDetails",
                model: "orderDetail",
                populate: {
                    path: "product",
                    model: "product",
                    select: "name amount",
                }
            },
        ]);

        if (updatedOrder) {
            return res.status(200).json({
                status:"Update order sucessfully",
                data: updatedOrder
            })
        } else {
            return res.status(404).json({
                status:"Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteOrder = async (req, res) => {
    //B1: Thu thập dữ liệu
    var orderId = req.params.orderId;
    var customerId = req.params.customerId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedOrder = await orderModel.findByIdAndDelete(orderId);
        // Nếu có customerId thì xóa thêm (optional)
        if(customerId !== undefined) {
            await customerModel.findByIdAndUpdate(customerId, {
                $pull: { orders: orderId }
            })
        }
        if (deletedOrder) {
            return res.status(200).json({
                status:"Delete order sucessfully",
                data: deletedOrder
            })
        } else {
            return res.status(404).json({
                status:"Not found any order"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllOrder,
    createOrder,
    getOrderById,
    updateOrder,
    deleteOrder
}

