const getAllProductMiddleware = (req, res, next) => {
    console.log("Get all product  Middleware");

    next();
}

const createProductMiddleware = (req, res, next) => {
    console.log("Create product  Middleware");

    next();
}

const getDetailProductMiddleware = (req, res, next) => {
    console.log("Get detail product  Middleware");

    next();
}

const updateProductMiddleware = (req, res, next) => {
    console.log("Update product  Middleware");

    next();
}

const deleteProductMiddleware = (req, res, next) => {
    console.log("Delete product  Middleware");

    next();
}

module.exports = {
    getAllProductMiddleware,
    createProductMiddleware,
    getDetailProductMiddleware,
    updateProductMiddleware,
    deleteProductMiddleware
}

