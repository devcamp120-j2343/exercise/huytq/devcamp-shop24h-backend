const expect = require("chai").expect;
const request = require("supertest");
const productTypesModel = require("../app/models/productType.model");
const app = require("../index");
const mongoose = require('mongoose');

describe("/api/v1/productTypes", () => {
    before(async () => {
        await productTypesModel.deleteMany({});
    });

    // after(async () => {
    //     mongoose.disconnect();
    // });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/Shop24h-Project");
    });

    describe("GET /", () => {
        it("should return all product type", async () => {
            const productType = [
                { name: "album", description: "album music of a singer"},
                { name: "t-shirt", description: "shirt with short hand-cut"},
            ];
            await productTypesModel.insertMany(productType);
            const res = await request(app).get("/api/v1/productTypes");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(2);
        });
    });

    describe("GET /:productTypeId", () => {
        it("should return a product type if valid id is passed", async () => {
            const productTypes = {
            _id:new mongoose.Types.ObjectId(),
            name: "sex tape",
            description: "18+",
        };
            await productTypesModel.create(productTypes);
            const res = await request(app).get("/api/v1/productTypes/" + productTypes._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("name", productTypes.name);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/api/v1/productTypes/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/api/v1/productTypes/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return product type when the all request body is valid", async () => {
            const res = await request(app)
                .post("/api/v1/productTypes")
                .send({ name: "magazine", description: "magazine of most famous article" });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("name", "magazine");
            expect(data.data).to.have.property("description", "magazine of most famous article");

            const productType = await productTypesModel.findOne({ name: 'magazine' });
            expect(productType.description).to.equal('magazine of most famous article');
        });
    });

    describe("PUT /:productTypeId", () => {
        it("should update the existing product type and return 200", async () => {
            const productType = {
                _id:new mongoose.Types.ObjectId(),
                name: "computer",
                description: "computer"
            };
            await productTypesModel.create(productType);

            const res = await request(app)
                .put("/api/v1/productTypes/" + productType._id)
                .send({ name: "laptop", description: "laptop" });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await productTypesModel.findById(productType._id).exec();
            expect(dataAfterUpdate).to.have.property("name", "laptop");
            expect(dataAfterUpdate).to.have.property("description", "laptop");
        });
    });

    let productTypeId = '';
    describe("DELETE /:productTypeId", () => {
        it("should delete requested id and return response 200", async () => {
            const productType = {
                _id:new mongoose.Types.ObjectId(),
                name: "tea",
                description: "sweet tea"
            };
            await productTypesModel.create(productType);
            productTypeId = productType._id;
            const res = await request(app).delete("/api/v1/productTypes/" + productTypeId);
            expect(res.status).to.be.equal(200);
        });

        it("should return 404 when deleted product type is requested", async () => {
            let res = await request(app).get("/api/v1/productTypes/" + productTypeId);
            expect(res.status).to.be.equal(404);
        });
    });
});