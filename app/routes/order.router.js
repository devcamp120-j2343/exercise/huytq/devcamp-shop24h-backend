const express = require("express");

const orderMiddleware = require("../middlewares/order.middleware");
const orderController = require("../controllers/order.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL order : ", req.url);

    next();
});

router.get("/", orderMiddleware.getAllOrderMiddleware, orderController.getAllOrder)

router.post("/", orderMiddleware.createOrderMiddleware, orderController.createOrder);

router.get("/:orderId", orderMiddleware.getDetailOrderMiddleware, orderController.getOrderById);

router.put("/:orderId", orderMiddleware.updateOrderMiddleware, orderController.updateOrder);

router.delete("/:orderId", orderMiddleware.deleteOrderMiddleware, orderController.deleteOrder);


module.exports = router;
