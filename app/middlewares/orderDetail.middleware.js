const getAllOrderDetailMiddleware = (req, res, next) => {
    console.log("Get all OrderDetail  Middleware");

    next();
}

const createOrderDetailMiddleware = (req, res, next) => {
    console.log("Create OrderDetail  Middleware");

    next();
}

const getDetailOrderDetailMiddleware = (req, res, next) => {
    console.log("Get detail OrderDetail  Middleware");

    next();
}

const updateOrderDetailMiddleware = (req, res, next) => {
    console.log("Update OrderDetail  Middleware");

    next();
}

const deleteOrderDetailMiddleware = (req, res, next) => {
    console.log("Delete OrderDetail  Middleware");

    next();
}

module.exports = {
    getAllOrderDetailMiddleware,
    createOrderDetailMiddleware,
    getDetailOrderDetailMiddleware,
    updateOrderDetailMiddleware,
    deleteOrderDetailMiddleware
}

