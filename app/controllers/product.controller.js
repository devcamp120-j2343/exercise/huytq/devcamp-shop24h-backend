//import thư viện mongoose
const mongoose = require('mongoose');

// import product model
const productModel = require('../models/product.model');

// const create product 
const createProduct = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        name,
        description,
        type,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount
    } = req.body;

    // B2: Validate dữ liệu
    
    if (!mongoose.Types.ObjectId.isValid(type)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Type Id is invalid!"
        })
    }

    if (!name) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "name is require"
        })
    }

    if (!description) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "description is require"
        })
    }

    if (!imageUrl) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "imageUrl is require"
        })
    }
    if (buyPrice === undefined || !(Number.isInteger(buyPrice) && buyPrice > 0 )) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "buyPrice is not valid"
        })
    }
    if (promotionPrice === undefined || !(Number.isInteger(promotionPrice) && promotionPrice > 0 && promotionPrice < buyPrice )) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "promotion price is not valid"
        })
    }
    if  (amount ===undefined || !(Number.isInteger(amount) && amount > 0)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "amount is not valid"
        })
    }


    // B3: Thao tác với CSDL
    var newProduct = {
        _id: new mongoose.Types.ObjectId(),
        name,
        type: new mongoose.Types.ObjectId(type),
        description,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount
    }

    try {
        const createdProduct = await productModel.create(newProduct);

        return res.status(201).json({
            status: "Create product successfully",
            data: createdProduct
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }

}

const getAllProduct = async (req, res) => {
    //B1: thu thập dữ liệu
     //B2: kiểm tra
    //B3: thực thi model
    try {
            const productList = await productModel.find().populate("type","name");

            if(productList && productList.length > 0) {
                return res.status(200).json({
                    status: "Get all product sucessfully",
                    data: productList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any product"
                })
            }
         
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getProductById = async (req, res) => {
    //B1: thu thập dữ liệu
    var productId = req.params.productId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const productInfo = await productModel.findById(productId).populate("type","name");

        if (productInfo) {
            return res.status(200).json({
                status:"Get product by id sucessfully",
                data: productInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any product "
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateProduct = async (req, res) => {
    //B1: thu thập dữ liệu
    var productId = req.params.productId;

    const {name,
        type,
        description,
        imageUrl,
        buyPrice,
        promotionPrice,
        amount} = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "name is require"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(type)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Type Id is invalid!"
        })
    }

    if (!description) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "description is require"
        })
    }

    if (!imageUrl) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "imageUrl is require"
        })
    }
    if (buyPrice === undefined || !(Number.isInteger(buyPrice) && buyPrice > 0 )) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "buyPrice is not valid"
        })
    }
    if (promotionPrice === undefined || !(Number.isInteger(promotionPrice) && promotionPrice > 0 && promotionPrice < buyPrice )) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "promotion price is not valid"
        })
    }
    if  (amount ===undefined || !(Number.isInteger(amount) && amount > 0)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "amount is not valid"
        })
    }

    //B3: thực thi model
    try {
        let updateProduct = {
            name,
            type: new mongoose.Types.ObjectId(type),
            description,
            imageUrl,
            buyPrice,
            promotionPrice,
            amount
        }

        const updatedProduct = await productModel.findByIdAndUpdate(productId, updateProduct).populate("type","name");

        if (updatedProduct) {
            return res.status(200).json({
                status:"Update product  sucessfully",
                data: updatedProduct
            })
        } else {
            return res.status(404).json({
                status:"Not found any product "
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteProduct = async (req, res) => {
    //B1: Thu thập dữ liệu
    var productId = req.params.productId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedProduct = await productModel.findByIdAndDelete(productId);

        if (deletedProduct) {
            return res.status(200).json({
                status:"Delete product sucessfully",
                data: deletedProduct
            })
        } else {
            return res.status(404).json({
                status:"Not found any product"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllProduct,
    createProduct,
    getProductById,
    updateProduct,
    deleteProduct
}

