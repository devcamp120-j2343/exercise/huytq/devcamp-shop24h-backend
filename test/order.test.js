const expect = require("chai").expect;
const request = require("supertest");
const orderModel = require("../app/models/order.model");
const customerModel = require("../app/models/customer.model");
const app = require("../index");
const mongoose = require('mongoose');

describe("/api/v1/orders", () => {
    var customerId = "";
    before(async () => {
        await orderModel.deleteMany({});
        //get tổng số lượng customer từ customer model
        const customerCount = await customerModel.count();
        //lấy random 1 số từ số tổng customer
        const random = await Math.floor(Math.random() * customerCount);
        //lấy 1 customer random từ customereModel
        const customerRandom = await customerModel.findOne().skip(random).exec();
        customerId = customerRandom._id;
    });

    // after(async () => {
    //     mongoose.disconnect();
    // });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/Shop24h-Project");
    });

    describe("GET /", () => {
        it("should return all order", async () => {
            const order = [
                {
                    orderDate: Date.now(),
                    shippedDate: Date.now(),
                    note: "abc",
                    orderDetails: [],
                    cost: 500,
                },
            ];
            await orderModel.insertMany(order);
            const res = await request(app).get("/api/v1/orders");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(1);
        });
    });

    describe("GET /:orderId", () => {
        it("should return a order if valid id is passed", async () => {
            const order = {
                _id: new mongoose.Types.ObjectId(),
                orderDate: Date.now(),
                shippedDate: Date.now(),
                note: "abc",
                orderDetails: [],
                cost: 500,
            };
            await orderModel.create(order);
            const res = await request(app).get("/api/v1/orders/" + order._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("note", order.note);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/api/v1/orders/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/api/v1/orders/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return order when the all request body is valid", async () => {
            const res = await request(app)
                .post("/api/v1/orders")
                .send({
                    customerId: customerId,
                    orderDate: Date.now(),
                    shippedDate: Date.now(),
                    note: "abc",
                    orderDetails: [],
                    cost: 500,
                });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("note", "abc");
            expect(data.data).to.have.property("cost", 500);
        });
    });

    describe("PUT /:orderId", () => {
        it("should update the existing order and return 200", async () => {
            const order = {
                _id: new mongoose.Types.ObjectId(),
                orderDate: Date.now(),
                shippedDate: Date.now(),
                note: "abc",
                orderDetails: [],
                cost: 500,
            };
            await orderModel.create(order);

            const res = await request(app)
                .put("/api/v1/orders/" + order._id)
                .send({
                    orderDate: Date.now(),
                    shippedDate: Date.now(),
                    note: "abcd",
                    orderDetails: [],
                    cost: 600,
                });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await orderModel.findById(order._id).exec();
            expect(dataAfterUpdate).to.have.property("note", "abcd");
            expect(dataAfterUpdate).to.have.property("cost", 600);
        });
    });

    let orderId = '';
    describe("DELETE /:orderId", () => {
        it("should delete requested id and return response 200", async () => {
            const order = {
                _id: new mongoose.Types.ObjectId(),
                orderDate: Date.now(),
                shippedDate: Date.now(),
                note: "abc",
                orderDetails: [],
                cost: 500,
            };
            await orderModel.create(order);
            orderId = order._id;
            const res = await request(app).delete("/api/v1/orders/" + orderId);
            expect(res.status).to.be.equal(200);
        });

        it("should return 404 when deleted order type is requested", async () => {
            let res = await request(app).get("/api/v1/orders/" + orderId);
            expect(res.status).to.be.equal(404);
        });
    });
});