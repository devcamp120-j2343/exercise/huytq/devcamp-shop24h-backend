const getAllProductTypeMiddleware = (req, res, next) => {
    console.log("Get all product type Middleware");

    next();
}

const createProductTypeMiddleware = (req, res, next) => {
    console.log("Create product type Middleware");

    next();
}

const getDetailProductTypeMiddleware = (req, res, next) => {
    console.log("Get detail product type Middleware");

    next();
}

const updateProductTypeMiddleware = (req, res, next) => {
    console.log("Update product type Middleware");

    next();
}

const deleteProductTypeMiddleware = (req, res, next) => {
    console.log("Delete product type Middleware");

    next();
}

module.exports = {
    getAllProductTypeMiddleware,
    createProductTypeMiddleware,
    getDetailProductTypeMiddleware,
    updateProductTypeMiddleware,
    deleteProductTypeMiddleware
}

