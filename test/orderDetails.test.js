const expect = require("chai").expect;
const request = require("supertest");
const orderDetailModel = require("../app/models/orderDetail.model");
const orderModel = require("../app/models/order.model");
const productModel = require("../app/models/product.model");
const app = require("../index");
const mongoose = require('mongoose');

describe("/api/v1/orderDetails", () => {
    var orderId = "";
    var productId = "";
    before(async () => {
        await orderDetailModel.deleteMany({});
        //get tổng số lượng order từ order model
        const orderCount = await orderModel.count();
        //lấy random 1 số từ số tổng order
        const random = await Math.floor(Math.random() * orderCount);
        //lấy 1 order random từ orderModel
        const orderRandom = await orderModel.findOne().skip(random).exec();
        orderId = orderRandom._id;
        //get tổng số lượng product từ product model
        const productCount = await productModel.count();
        //lấy random 1 số từ số tổng product
        const randomm = await Math.floor(Math.random() * productCount);
        //lấy 1 order random từ orderModel
        const productRandom = await orderModel.findOne().skip(randomm).exec();
        productId = productRandom._id;
    });

    after(async () => {
        mongoose.disconnect();
    });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/Shop24h-Project");
    });

    describe("GET /", () => {
        it("should return all order detail", async () => {
            const orderDetails = [
                {
                    product: productId,
                    quantity: 50,
                },
            ];
            await orderDetailModel.insertMany(orderDetails);
            const res = await request(app).get("/api/v1/orderDetails");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(1);
        });
    });

    describe("GET /:orderDetailId", () => {
        it("should return a order detail if valid id is passed", async () => {
            const orderDetail = {
                _id: new mongoose.Types.ObjectId(),
                product: productId,
                quantity: 40,
            };
            await orderDetailModel.create(orderDetail);
            const res = await request(app).get("/api/v1/orderDetails/" + orderDetail._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("quantity", orderDetail.quantity);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/api/v1/orderDetails/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/api/v1/orderDetails/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return order when the all request body is valid", async () => {
            const res = await request(app)
                .post("/api/v1/orderDetails")
                .send({
                    orderId: orderId,
                    product: productId,
                    quantity: 500,
                });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("quantity", 500);
        });
    });

    describe("PUT /:orderDetailId", () => {
        it("should update the existing order detail and return 200", async () => {
            const orderDetail = {
                _id: new mongoose.Types.ObjectId(),
                product: productId,
                quantity: 1000,
            };
            await orderDetailModel.create(orderDetail);

            const res = await request(app)
                .put("/api/v1/orderDetails/" + orderDetail._id)
                .send({
                    product: productId,
                    quantity: 2000,
                });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await orderDetailModel.findById(orderDetail._id).exec();
            expect(dataAfterUpdate).to.have.property("quantity", 2000);
        });
    });

    let orderDetailId = '';
    describe("DELETE /:orderDetailId", () => {
        it("should delete requested id and return response 200", async () => {
            const orderDetail = {
                _id: new mongoose.Types.ObjectId(),
                product: productId,
                quantity: 10,
            };
            await orderDetailModel.create(orderDetail);
            orderDetailId = orderDetail._id;
            const res = await request(app).delete("/api/v1/orderDetails/" + orderDetailId);
            expect(res.status).to.be.equal(200);
        });

        it("should return 404 when deleted order detail is requested", async () => {
            let res = await request(app).get("/api/v1/orderDetails/" + orderDetailId);
            expect(res.status).to.be.equal(404);
        });
    });
});