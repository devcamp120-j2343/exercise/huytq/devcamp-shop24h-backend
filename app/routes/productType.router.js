const express = require("express");

const productTypeMiddleware = require("../middlewares/productType.middlewares");
const productTypeController = require("../controllers/productType.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL product type: ", req.url);

    next();
});

router.get("/", productTypeMiddleware.getAllProductTypeMiddleware, productTypeController.getAllProductType)

router.post("/", productTypeMiddleware.createProductTypeMiddleware, productTypeController.createProductType);

router.get("/:productTypeId", productTypeMiddleware.getDetailProductTypeMiddleware, productTypeController.getProductTypeById);

router.put("/:productTypeId", productTypeMiddleware.updateProductTypeMiddleware, productTypeController.updateProductType);

router.delete("/:productTypeId", productTypeMiddleware.deleteProductTypeMiddleware, productTypeController.deleteProductType);


module.exports = router;
