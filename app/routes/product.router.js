const express = require("express");

const productMiddleware = require("../middlewares/product.middleware");
const productController = require("../controllers/product.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL product : ", req.url);

    next();
});

router.get("/", productMiddleware.getAllProductMiddleware, productController.getAllProduct)

router.post("/", productMiddleware.createProductMiddleware, productController.createProduct);

router.get("/:productId", productMiddleware.getDetailProductMiddleware, productController.getProductById);

router.put("/:productId", productMiddleware.updateProductMiddleware, productController.updateProduct);

router.delete("/:productId", productMiddleware.deleteProductMiddleware, productController.deleteProduct);


module.exports = router;
