//Import thư viện ExpressJS
// import express from 'express';
const express = require('express');

//import thư viện mongoose
const mongoose = require('mongoose');

// Khởi tạo app nodejs bằng express
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;

//khai bao router
const productTypeRouter = require("./app/routes/productType.router");
const productRouter = require("./app/routes/product.router");
const customerRouter = require("./app/routes/customer.router");
const orderRouter = require("./app/routes/order.router");
const orderDetailRouter  = require("./app/routes/orderDetail.router");
// Khai báo ứng dụng đọc được body raw json (Build in middleware)
app.use(express.json());

//Middleware in ra console thời gian hiện tại mỗi khi API gọi
app.use((request, response, next) => {
    var today = new Date();

    console.log("Current time: ", today);

    next();
});

//Middleware in ra console request method mỗi khi API gọi
app.use((request, response, next) => {
    console.log("Method: ", request.method);

    next();
});


//ket noi database mongo
mongoose.connect("mongodb://127.0.0.1:27017/Shop24h-Project")
  .then(() => console.log("Connected to Mongo Successfully"))
  .catch(error => handleError(error));

//khai báo các model
const productTypeModel = require('./app/models/productType.model');
const productModel = require('./app/models/product.model');
const customerModel = require('./app/models/customer.model');
const orderModel = require('./app/models/order.model');
const orderDetailModel = require('./app/models/orderDetail.model');
//khai bao api 
app.use("/api/v1/productType", productTypeRouter);
app.use("/api/v1/product",productRouter);
app.use("/api/v1/customer",customerRouter);
app.use("/api/v1/order",orderRouter);
app.use("/api/v1/orderDetail",orderDetailRouter);
// Chạy ứng dụng
app.listen(port, () => {
    console.log("Ứng dụng chạy trên cổng: ", port);
})
module.exports = app;