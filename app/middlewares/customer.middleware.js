const getAllCustomerMiddleware = (req, res, next) => {
    console.log("Get all Customer  Middleware");

    next();
}

const createCustomerMiddleware = (req, res, next) => {
    console.log("Create Customer  Middleware");

    next();
}

const getDetailCustomerMiddleware = (req, res, next) => {
    console.log("Get detail Customer  Middleware");

    next();
}

const updateCustomerMiddleware = (req, res, next) => {
    console.log("Update Customer  Middleware");

    next();
}

const deleteCustomerMiddleware = (req, res, next) => {
    console.log("Delete Customer  Middleware");

    next();
}

module.exports = {
    getAllCustomerMiddleware,
    createCustomerMiddleware,
    getDetailCustomerMiddleware,
    updateCustomerMiddleware,
    deleteCustomerMiddleware
}

