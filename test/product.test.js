const expect = require("chai").expect;
const request = require("supertest");
const productModel = require("../app/models/product.model");
const productTypeModel = require("../app/models/productType.model");
const app = require("../index");
const mongoose = require('mongoose');

describe("/api/v1/products", () => {
    var productTypeId = "";
    before(async () => {
        await productModel.deleteMany({});
        //get tổng số lượng product type từ product type model
        const productTypeCount = await productTypeModel.count();
        //lấy random 1 số từ số tổng productType
        const random = await Math.floor(Math.random() * productTypeCount);
        //lấy 1 product type random từ productTypeModel
        const productTypeRandom = await productTypeModel.findOne().skip(random).exec();
        productTypeId = productTypeRandom._id;
    });

    // after(async () => {
    //     mongoose.disconnect();
    // });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/Shop24h-Project");
    });

    describe("GET /", () => {
        it("should return all product", async () => {
            const product = [
                {
                    name: "SmartPhone",
                    description: "Ihone 10",
                    type: productTypeId,
                    imageUrl: "imageUrl",
                    buyPrice: 300,
                    promotionPrice: 280,
                    amount: 10,
                },
            ];
            await productModel.insertMany(product);
            const res = await request(app).get("/api/v1/products");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(1);
        });
    });

    describe("GET /:productId", () => {
        it("should return a product if valid id is passed", async () => {
            const product = {
                _id: new mongoose.Types.ObjectId(),
                name: "Spiderman 2",
                description: "Marvel movie",
                type: productTypeId,
                imageUrl: "imageUrl",
                buyPrice: 300,
                promotionPrice: 280,
                amount: 10,
            };
            await productModel.create(product);
            const res = await request(app).get("/api/v1/products/" + product._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("name", product.name);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/api/v1/products/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/api/v1/products/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return product when the all request body is valid", async () => {
            const res = await request(app)
                .post("/api/v1/products")
                .send({ name: "God Of War",
                description: "Ps5 game",
                type: productTypeId,
                imageUrl: "imageUrl",
                buyPrice: 300,
                promotionPrice: 280,
                amount: 10, });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("name", "God Of War");
            expect(data.data).to.have.property("description", "Ps5 game");

            const product = await productModel.findOne({ name: 'God Of War' });
            expect(product.type.toString()).to.equal(productTypeId.toString());
        });
    });

    describe("PUT /:productId", () => {
        it("should update the existing product and return 200", async () => {
            const product = {
                _id: new mongoose.Types.ObjectId(),
                name: "obama",
                description: "President USA ",
                type: productTypeId,
                imageUrl: "imageUrl",
                buyPrice: 300,
                promotionPrice: 280,
                amount: 10,
            };
            await productModel.create(product);

            const res = await request(app)
                .put("/api/v1/products/" + product._id)
                .send({ name: "binladen",
                description: "IS",
                type: productTypeId,
                imageUrl: "imageUrl",
                buyPrice: 300,
                promotionPrice: 280,
                amount: 10, });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await productModel.findById(product._id).exec();
            expect(dataAfterUpdate).to.have.property("name", "binladen");
            expect(dataAfterUpdate).to.have.property("description", "IS");
        });
    });

    let productId = '';
    describe("DELETE /:productId", () => {
        it("should delete requested id and return response 200", async () => {
            const productType = {
                _id: new mongoose.Types.ObjectId(),
                name: "hay trao cho anh",
                description: "Son Tung's album",
                type: productTypeId,
                imageUrl: "imageUrl",
                buyPrice: 300,
                promotionPrice: 280,
                amount: 10,
            };
            await productModel.create(productType);
            productId = productType._id;
            const res = await request(app).delete("/api/v1/products/" + productId);
            expect(res.status).to.be.equal(200);
        });

        it("should return 404 when deleted product type is requested", async () => {
            let res = await request(app).get("/api/v1/products/" + productId);
            expect(res.status).to.be.equal(404);
        });
    });
});