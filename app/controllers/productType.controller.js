//import thư viện mongoose
const mongoose = require('mongoose');

// import productType model
const productTypeModel = require('../models/productType.model');

//import product model
const productModel = require('../models/product.model');

// const create review 
const createProductType = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        productId,
        name,
        description
    } = req.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "product ID is not valid"
        })
    }

    if (!name) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "name is require"
        })
    }

    if (!description) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "description is require"
        })
    }

    // B3: Thao tác với CSDL
    var newProductType = {
        _id: new mongoose.Types.ObjectId(),
        name,
        description
    }

    try {
        const createdProductType = await productTypeModel.create(newProductType);
        
        const updatedProduct = await productModel.findByIdAndUpdate(productId, {
            $push: { type: createdProductType._id }
        })

        return res.status(201).json({
            status: "Create product type successfully",
            product: updatedProduct,
            data: createdProductType
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllProductType = async (req, res) => {
    //B1: thu thập dữ liệu
    const productId = req.query.productId;
    //B2: kiểm tra
    if (productId !== undefined && !mongoose.Types.ObjectId.isValid(productId)) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "productId  is not valid"
        })
    }
    //B3: thực thi model
    try {
        if(productId === undefined) {
            const productTypeList = await productTypeModel.find();

            if(productTypeList && productTypeList.length > 0) {
                return res.status(200).json({
                    status: "Get all product type sucessfully",
                    data: productTypeList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any product type"
                })
            }
        } else {
            const productInfo = await productModel.findById(productId).populate("type");

            return res.status(200).json({
                status: "Get all product type of product sucessfully",
                data: productInfo.type
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getProductTypeById = async (req, res) => {
    //B1: thu thập dữ liệu
    var productTypeId = req.params.productTypeId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const productTypeInfo = await productTypeModel.findById(productTypeId);

        if (productTypeInfo) {
            return res.status(200).json({
                status:"Get product type by id sucessfully",
                data: productTypeInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any product type"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateProductType = async (req, res) => {
    //B1: thu thập dữ liệu
    var productTypeId = req.params.productTypeId;

    const {name, description} = req.body;

    //B2: kiểm tra dữ liệu
    if (!name) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "name is require"
        })
    }

    if (!description) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "description is require"
        })
    }

    //B3: thực thi model
    try {
        let updateProductType = {
            name,
            description
        }

        const updatedProductType = await productTypeModel.findByIdAndUpdate(productTypeId, updateProductType);

        if (updatedProductType) {
            return res.status(200).json({
                status:"Update product type sucessfully",
                data: updatedProductType
            })
        } else {
            return res.status(404).json({
                status:"Not found any product type"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteProductType = async (req, res) => {
    //B1: Thu thập dữ liệu
    var productTypeId = req.params.productTypeId;
    var productId = req.params.productId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(productTypeId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedProductType = await productTypeModel.findByIdAndDelete(productTypeId);
        // Nếu có productId thì xóa thêm (optional)
        if(productId !== undefined) {
            await productModel.findByIdAndUpdate(productId, {
                $pull: { type: productTypeId }
            })
        }
        if (deletedProductType) {
            return res.status(200).json({
                status:"Delete product type sucessfully",
                data: deletedProductType
            })
        } else {
            return res.status(404).json({
                status:"Not found any product type"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllProductType,
    createProductType,
    getProductTypeById,
    updateProductType,
    deleteProductType
}

