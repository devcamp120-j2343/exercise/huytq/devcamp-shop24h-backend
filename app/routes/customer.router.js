const express = require("express");

const customerMiddleware = require("../middlewares/customer.middleware");
const customerController = require("../controllers/customer.controller");
const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL customer : ", req.url);

    next();
});

router.get("/", customerMiddleware.getAllCustomerMiddleware, customerController.getAllCustomer)

router.post("/", customerMiddleware.createCustomerMiddleware, customerController.createCustomer);

router.get("/:customerId", customerMiddleware.getDetailCustomerMiddleware, customerController.getCustomerById);

router.put("/:customerId", customerMiddleware.updateCustomerMiddleware, customerController.updateCustomer);

router.delete("/:customerId", customerMiddleware.deleteCustomerMiddleware, customerController.deleteCustomer);


module.exports = router;
