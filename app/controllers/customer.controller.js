//import thư viện mongoose
const mongoose = require('mongoose');

// import Customer model
const customerModel = require('../models/customer.model');

// const create customer 
const createCustomer = async (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders,
    } = req.body;

    // B2: Validate dữ liệu
    
    if (!fullName) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "fullName is require"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orders)) {
        return res.status(400).json({
            status: "Bad request",
            message: "order Id is invalid!"
        })
    }

    if (!phone) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "phone is require"
        })
    }

    if (!email) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "email is require"
        })
    }

    
    if (!address) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "address is require"
        })
    }

    if (!city) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "city is require"
        })
    }

    if (!country) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "country is require"
        })
    }

    // B3: Thao tác với CSDL
    var newCustomer = {
        _id: new mongoose.Types.ObjectId(),
        fullName,
        phone,
        email,
        address,
        city,
        country,
        orders: new mongoose.Types.ObjectId(orders)
    }

    try {
        const createdCustomer = await customerModel.create(newCustomer).populate(orders);
        return res.status(201).json({
            status: "Create customer successfully",
            data: createdCustomer
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: err.message
        })
    }
}

const getAllCustomer = async (req, res) => {
    //B1: thu thập dữ liệu
    //B2: kiểm tra
    //B3: thực thi model
    try {
            const customerList = await customerModel.find().populate("orders");

            if(customerList && customerList.length > 0) {
                return res.status(200).json({
                    status: "Get all customer sucessfully",
                    data: customerList
                })
            } else {
                return res.status(404).json({
                    status: "Not found any customer"
                })
            }
        
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getCustomerById = async (req, res) => {
    //B1: thu thập dữ liệu
    var customerId = req.params.customerId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }
    //B3: thực thi model
    try {
        const customerInfo = await customerModel.findById(customerId).populate("orders");

        if (customerInfo) {
            return res.status(200).json({
                status:"Get customer by id sucessfully",
                data: customerInfo
            })
        } else {
            return res.status(404).json({
                status:"Not found any customer"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

const updateCustomer = async (req, res) => {
    //B1: thu thập dữ liệu
    var customerId = req.params.customerId;

    const {fullName,
        phone,
        email,
        address,
        city,
        country,
        orders
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "fullName is require"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orders)) {
        return res.status(400).json({
            status: "Bad request",
            message: "order Id is invalid!"
        })
    }

    if (!phone) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "phone is require"
        })
    }

    if (!email) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "email is require"
        })
    }

    
    if (!address) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "address is require"
        })
    }

    if (!city) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "city is require"
        })
    }

    if (!country) {
        return res.status(400).json({
            "status": "Bad Request",
            "message": "country is require"
        })
    }

    //B3: thực thi model
    try {
        let updateCustomer = {
            fullName,
            phone,
            email,
            address,
            city,
            country,
            orders: new mongoose.Types.ObjectId(orders)
        }

        const updatedCustomer = await customerModel.findByIdAndUpdate(customerId, updateCustomer).populate(orders);

        if (updatedCustomer) {
            return res.status(200).json({
                status:"Update customer sucessfully",
                data: updatedCustomer
            })
        } else {
            return res.status(404).json({
                status:"Not found any customer"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}


const deleteCustomer = async (req, res) => {
    //B1: Thu thập dữ liệu
    var customerId = req.params.customerId;
    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return res.status(400).json({
            status:"Bad request",
            message:"Id is invalid!"
        })
    }

    try {
        const deletedCustomer = await customerModel.findByIdAndDelete(customerId);
    
        if (deletedCustomer) {
            return res.status(200).json({
                status:"Delete customer sucessfully",
                data: deletedCustomer
            })
        } else {
            return res.status(404).json({
                status:"Not found any customer"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status:"Internal Server Error",
            message:error.message
        })
    }
}

module.exports = {
    getAllCustomer,
    createCustomer,
    getCustomerById,
    updateCustomer,
    deleteCustomer
}

